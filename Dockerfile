FROM openjdk:12-alpine
VOLUME /tmp
ADD ./build/libs/*.jar theme-park-ride-gradle.jar
EXPOSE 5000
ENTRYPOINT exec java -jar theme-park-ride-gradle.jar
HEALTHCHECK --interval=20s --timeout=3s --retries=3 CMD curl --fail http://localhost:8080/actuator/health || exit 1
